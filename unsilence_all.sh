if [ ! -d unsilence/ ]; then
  mkdir -p unsilence;
fi
for f in *.mp4; do
	echo calculating "$f":;
	/home/tuff/.local/bin/unsilence "$f" unsilence/speed_"$f" -as 1.2 -ss 8 -t 16 -y;
done
